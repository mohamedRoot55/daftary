import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../Models/Transactions.dart';
import './Chart_bar.dart';

class Chart extends StatelessWidget {
  final List<Transactions> currentTransactions;

  Chart(this.currentTransactions);

  List<Map<String, Object>> get groupedTransactionValues {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(
        Duration(days: index),
      );
      var totalSum = 0.0;

      for (var i = 0; i < currentTransactions.length; i++) {
        if (currentTransactions[i].dateTime.day == weekDay.day &&
            currentTransactions[i].dateTime.month == weekDay.month &&
            currentTransactions[i].dateTime.year == weekDay.year) {
          totalSum += currentTransactions[i].price;
        }
      }
      //print(totalSum);

      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalSum,
      };
    }).reversed.toList();
  }


  double get totalSpending {
    return groupedTransactionValues.fold(0.0, (sum, item) {
    //  print(sum + item['amount']);
      return sum + item['amount'];
    });

  }

  @override
  Widget build(BuildContext context) {

     return Card(

        elevation: 6,
        margin: EdgeInsets.all(20),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment:  MainAxisAlignment.spaceAround,
            children: groupedTransactionValues.map((data) {

               //print(data['amount']) ;bv
              return Flexible(
                fit: FlexFit.tight,
                child: Chart_bar(
                   data['day'],
                  data['amount'],
                    totalSpending == 0.0 ? 0.0 : (data['amount'] as double) / totalSpending
                ),
              );
            }).toList(),
          ),
        ),
      );
  }
}
