import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Models/Transactions.dart';
import 'package:intl/intl.dart';

class ListOfTransactions extends StatelessWidget {
  final List<Transactions> transaction;
  final Function removeTransaction;

  ListOfTransactions(this.transaction, this.removeTransaction);

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: MediaQuery.of(context).size.height * 0.6,

      child: transaction.isEmpty
          ? LayoutBuilder(
              builder: (ctx, constraints) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      'no transactions added yet',
                      style: TextStyle(color: Colors.purple, fontSize: 20),
                    ),
                    Container(
                        height: constraints.maxHeight * 0.6,
                        child: Image.asset('assets/images/waiting.png'))
                  ],
                );
              },
            )
          : ListView.builder(
              itemBuilder: (context, index) {
                return Card(
                  elevation: 5,
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                  child: ListTile(
                    leading: CircleAvatar(
                      radius: 30,
                      backgroundColor: Colors.purple,
                      child: Padding(
                          padding: EdgeInsets.all(6),
                          child: FittedBox(
                              child: Text(
                            '\$${transaction[index].price}',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ))),
                    ),
                    title: Text(transaction[index].title),
                    subtitle: Text(
                        DateFormat.yMMMd().format(transaction[index].dateTime)),
                    trailing: MediaQuery.of(context).size.width > 400
                        ? FlatButton.icon(
                            onPressed: () =>
                                removeTransaction(transaction[index].id),
                            icon: Icon(Icons.delete_forever),
                            label: Text(
                              'Delete',
                              style: TextStyle(color: Colors.red),
                            ))
                        : IconButton(
                            icon: Icon(Icons.delete_forever),
                            color: Colors.red,
                            onPressed: () =>
                                removeTransaction(transaction[index].id),
                          ),
                  ),
                );
              },
              itemCount: transaction.length,
            ),
    );
  }
}
