import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
class New_Transaction extends StatefulWidget {
  Function add_new_Transaction;

  New_Transaction(this.add_new_Transaction);

  @override
  State createState() => _New_TransactionState();
}

class _New_TransactionState extends State<New_Transaction> {
  final titleController = new TextEditingController();
  DateTime date ; 
  final priceController = new TextEditingController();

  void submit() {
    final String title = titleController.text;
    final double price = double.parse(priceController.text);
    if (title.isEmpty || price <= 0 || date == null) {
      return;
    }
    widget.add_new_Transaction(title, price , date);
    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2019),
            lastDate: DateTime.now())
        .then((pickedDate) {
          if(pickedDate == null){
            return ; 
          }
          setState(() {
            date = pickedDate ;
          });

    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          TextField(
            //  style: TextStyle(color: Colors.purple),
            decoration: InputDecoration(
                labelText: 'product', contentPadding: EdgeInsets.all(15)),
            controller: titleController,
            onSubmitted: (_) {
              submit();
            },
          ),
          TextField(
            //  style: TextStyle(color: Colors.purple),
            decoration: InputDecoration(
                labelText: 'price', contentPadding: EdgeInsets.all(15)),
            controller: priceController,
            onSubmitted: (_) {
              submit();
            },
          ),
          Container(
            height: 70,
            child: Row(
              children: <Widget>[
                
                Expanded(child: Text( date == null ?'no date slected yet' : 'picked date   ${ DateFormat.yMd().format(date)}')),
                FlatButton(
                  child: Text(
                    'add date',
                    style: TextStyle(
                        color: Colors.purple, fontWeight: FontWeight.bold),
                  ),
                  onPressed: _presentDatePicker,
                ),
              ],
            ),
          ),
          RaisedButton(
              color: Colors.purple,
              child: Text(
                'add new transaction',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: submit)
        ],
      ),
    );
  }
}
