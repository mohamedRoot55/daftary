class Transactions {
  String title;

  double price;

  DateTime dateTime;

  String id = DateTime.now().toString();

  Transactions({this.title, this.price, this.dateTime, this.id});
}
